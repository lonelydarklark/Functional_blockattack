package block.gui.ending;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import block.enums.GameScene;
import block.enums.Panels;
import block.gui.GameStateKeeper;

@SuppressWarnings("serial")
public class EndingPanel extends JPanel{

	private GameScene scene;
	private final JLabel centerMes;


	public EndingPanel(GameStateKeeper key){
		this.setName(Panels.END.toString());
		this.addKeyListener(key);

		centerMes = new JLabel("", SwingConstants.CENTER);
		centerMes.setSize(new Dimension(WIDTH, HEIGHT));
		centerMes.setLocation(0, getHeight()/2);

		scene = GameScene.GAMECLEAR;


		this.add(centerMes);
	}

	private void endingUpdate(){
//		if(key.isState(KeyStates.ENTER)){
//			centerMes.setText("");
//			init();
//		}
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		g.setColor(Color.black);
		switch(scene){

		case GAMECLEAR:
			centerMes.setText("GAMECLEAR!");
			break;
		case GAMEOVER:
			centerMes.setText("GAMEOVER...");
			break;
		default:
			break;
		}
	}

//	@Override
//	public void update() {
//		// TODO 自動生成されたメソッド・スタブ
//
//	}
}
