/**
 *
 */
package block.gui.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import block.enums.Panels;
import block.gui.GameStateKeeper;

@SuppressWarnings("serial")
public class GamePanel extends JPanel{
	public static final int WIDTH = 960;
	public static final int HEIGHT = 1280;

	private JLabel centerMes;
	private JLabel lastGames;
	private JLabel lastBlocks;

	public GameStateKeeper key;

	public GamePanel(GameStateKeeper key){
		this.setName(Panels.GAME.toString());

		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);

		centerMes = new JLabel("", SwingConstants.CENTER);
		centerMes.setSize(new Dimension(WIDTH, HEIGHT));
		centerMes.setLocation(0, getHeight()/2);
		add(centerMes);

		lastGames = new JLabel("", SwingConstants.RIGHT);
		lastGames.setSize(new Dimension(WIDTH, 10));
		lastGames.setLocation(0, HEIGHT-110);
		add(lastGames);

		lastBlocks = new JLabel("", SwingConstants.RIGHT);
		lastBlocks.setSize(new Dimension(WIDTH, 10));
		lastBlocks.setLocation(0, HEIGHT-100);
//		System.out.println(HEIGHT-100);
		add(lastBlocks);
		setLayout(null);

		this.key = key;
		this.addKeyListener(this.key);


		new Thread(key).start();
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
//		System.out.println("game panel paint...");
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());

		key.getGameState().draw(g);
	}
}
