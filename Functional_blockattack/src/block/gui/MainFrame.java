package block.gui;

import java.awt.CardLayout;
import java.awt.Container;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import block.enums.Panels;
import block.gui.ending.EndingPanel;
import block.gui.game.GamePanel;
import block.gui.speed_select.SpeedSelectPanel;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements Runnable{

	public static final int FRAME_RATE = 0;

	public final List<JPanel> panels;
	public final GameStateKeeper key;

	final Container container = this.getContentPane();
	final CardLayout layout = new CardLayout();

	public MainFrame(){
		key = new GameStateKeeper();

		this.panels = new ArrayList<>();
//				panels.add(new SpeedSelectPanel(key));
				panels.add(new GamePanel(key));
//				panels.add(new EndingPanel(key));
		panels.forEach(container::add);

//        key.ifPresent(container::addKeyListener);

        this.setLayout(layout);
//        layout.first(container);
        layout.show(container, Panels.GAME.toString());
        init.run();

        new Thread(this).start();
	}

	private final Runnable nextPanel = () -> {
		layout.next(container);
	};

	private final Runnable initLookFeel = () -> {
		try{
			String look = UIManager.getSystemLookAndFeelClassName();
			UIManager.setLookAndFeel(look);
		}catch(Exception e){
			e.printStackTrace();
		}
	};

	private final Runnable initFrame = () -> {
		this.setTitle("");
//		this.setLocation(200, 40);
		this.setResizable(false); //最大化可能か不可能か
		this.setSize(240, 320);

        pack();//サイズ調整
		setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

	};

	private final Runnable init = () -> {
        initLookFeel.run();
        initFrame.run();
	};

	@Override
	public void run() {
		while(true){
			try{
				panels.forEach(p -> p.repaint());
				repaint();

				Thread.sleep(FRAME_RATE);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
