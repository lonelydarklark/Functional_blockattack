package block.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import block.enums.GameSpeed;
import block.enums.KeyStates;
import block.game.GameManager;
import block.game.states.GameState;

public class GameStateKeeper implements Runnable, KeyListener{

	private GameState game = GameManager.newGameState.apply(GameSpeed.NORMAL);
	private GameManager gameManager = new GameManager();

	@Override
	public void keyPressed(KeyEvent e){
		switch(KeyStates.valueOf(e)){
		case LEFT:
			game = gameManager.leftUpdate.apply(game);
			break;

		case RIGHT:
			game = gameManager.rightUpdate.apply(game);
			break;

		case SPACE:
			game = gameManager.spaceUpdate.apply(game);
			break;

		case ENTER:
			game = gameManager.enterUpdate.apply(game);
			break;

		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e){
		//no use
	}

	@Override
	public void keyTyped(KeyEvent e){
		//no use
	}

	public GameState getGameState(){
		return this.game;
	}

	@Override
	public void run(){
		while(true){
			try{
				game = gameManager.regularUpdate.apply(game);

				Thread.sleep(game.gameSpeed.sleepTime);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
}
