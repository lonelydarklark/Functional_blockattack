package block.gui.speed_select;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import block.enums.Panels;
import block.gui.GameStateKeeper;
import block.interfaces.Updatable;

@SuppressWarnings("serial")
public class SpeedSelectPanel extends JPanel{

	private JLabel centerMes;

	public SpeedSelectPanel(GameStateKeeper key) {
		this.setName(Panels.SPEED_SELECT.toString());
		this.addKeyListener(key);

		centerMes = new JLabel("", SwingConstants.CENTER);
		centerMes.setSize(new Dimension(WIDTH, HEIGHT));
		centerMes.setLocation(0, getHeight()/2);
		add(centerMes);
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		g.setColor(Color.WHITE);
		g.fillRect(0, 0, WIDTH, HEIGHT);

		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 100, 100);
	}

	private void speedMenuUpdate(){
//
//		centerMes.setText("SPEED: [1]LOW [2]NORMAL [3]HIGH");
//
//		if(key.isState(KeyStates.ONE)) {
//			game.setGameSpeed(GameSpeed.LOW);
//		}else if(key.isState(KeyStates.TWO)) {
//			game.setGameSpeed(GameSpeed.NORMAL);
//		}else if(key.isState(KeyStates.THREE)) {
//			game.setGameSpeed(GameSpeed.HIGH);
//		}
//
//		if(key.isState(KeyStates.ENTER)){
//			centerMes.setText("");
//			scene = Scene.PLAYING_S;
//		}
	}
}
