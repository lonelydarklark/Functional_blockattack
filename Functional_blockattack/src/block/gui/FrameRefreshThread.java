package block.gui;

import javax.swing.JFrame;

import block.game.states.AbstractField;

public class FrameRefreshThread extends Thread {

	private JFrame frame;
	private AbstractField field;

	public FrameRefreshThread(JFrame frame, AbstractField field){
		this.frame = frame;
		this.field = field;

		this.start();
	}

	public void run(){
//		while(true){
//			this.field.update();
//			this.frame.repaint();
//		}
	}
}
