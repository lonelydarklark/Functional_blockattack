package block.enums;

public enum Scene{
	NONE,
	SPEED_SELECT,
	PLAYING_S,
	GAMECLEAR_S,
	GAMEOVER_S;
}
