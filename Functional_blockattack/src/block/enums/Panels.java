package block.enums;

public enum Panels {

	SPEED_SELECT("SpeedSelectPanel"),
	GAME("GamePanel"),
	END("EndPanel"),
	;

	private String name;
	private Panels(String name){
		this.name = name;
	}

	public String toString(){
		return name;
	}
}
