package block.enums;

import java.awt.event.KeyEvent;

public enum KeyStates {
	NONE(-1),
	LEFT(KeyEvent.VK_LEFT),
	RIGHT(KeyEvent.VK_RIGHT),
	ONE(KeyEvent.VK_1),
	TWO(KeyEvent.VK_2),
	THREE(KeyEvent.VK_3),
	SPACE(KeyEvent.VK_SPACE),
	ENTER(KeyEvent.VK_ENTER),
	;
	private int keyCode;
	private KeyStates(int keyCode){
		this.keyCode = keyCode;
	}

	public static KeyStates valueOf(KeyEvent e){
		for(KeyStates s : values()){
			if(s.keyCode == e.getKeyCode()){
				return s;
			}
		}
		return NONE;
	}
}
