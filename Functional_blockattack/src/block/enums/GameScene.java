package block.enums;

public enum GameScene {
	WAIT,
	PLAYING,
	GAMECLEAR,
	GAMEOVER;
}
