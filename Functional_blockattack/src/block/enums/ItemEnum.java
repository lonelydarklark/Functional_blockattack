package block.enums;

public enum ItemEnum {
	POWER(1),
	SPEED(2),
	BALL(3),
	RACKET(4),
	;
	public int num;
	private ItemEnum(int num){
		this.num = num;
	}
	public static ItemEnum valueOf(int num){
		for(ItemEnum i : values()){
			if(i.num == num){
				return i;
			}
		}
		return null;
	}
}
