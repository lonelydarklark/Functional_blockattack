package block.enums;

public enum RacketKeys{
	X("racket_x"),
	Y("racket_y"),
	VX("racket_vx"),
	VY("racket_vy"),
	WIDTH("racket_width"),
	Height("racket_height"),
	WIDE_RANGE("racket_wideRange"),
	TIME_COUNT("racket_time_count"),
	WIDE_MODE("racket_wide_mode"),
	;
	private String key;
	private RacketKeys(String key){
		this.key = key;
	}
	@Override
	public String toString(){
		return key;
	}
}
