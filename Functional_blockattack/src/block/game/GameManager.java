package block.game;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import block.enums.GameSpeed;
import block.game.manager.BallsManager;
import block.game.manager.BlocksManager;
import block.game.manager.ItemManager;
import block.game.manager.RacketManager;
import block.game.states.GameState;

public class GameManager{
	public static final int FRAME_RATE = 100;
	private final int MAX_ROUND = 3;
	private final RacketManager racket = new RacketManager();
	private final BallsManager balls = new BallsManager();
	private final BlocksManager blocks = new BlocksManager();
	private final ItemManager items = new ItemManager();
	//	private int roundCount;
	//	private GameSpeed gameSpeed;

	//	private boolean havePowerItem;
	//	private boolean haveBallItem;
	//	private boolean missed;

	//	private final String message = "";
	//	private final String lastGamesMes = "";
	//	private final String existBlocksMes = "";

	//	public final GameState state;

	public GameManager(){
		//		missed = false;
		//		balls.add(new BallLogic(racket.getCenterX()-BallLogic.SIZE/2, racket.getY()-BallLogic.SIZE));
		//		havePowerItem = false;
		//		haveBallItem = false;
		//		roundCount = 0;

		//		state.map(newGameState);
		//		state.blocks.addAll(BlocksManager.stage1.get());
		//		state.racket.add(RacketManager.newDefaultRacket.get());
		//		state.balls.add(BallsManager.newBall.get());

	}

	public static final Function<GameSpeed, GameState> newGameState = speed ->
	new GameState(BallsManager.newBall.get(),
			BlocksManager.stage1.get(),
			RacketManager.newDefaultRacket.get(),
			ItemManager.emptyItems.get(),
			speed, 0, false);

	private final Predicate<GameState> isGameClear = g -> g.blocks.stream().count() <= 0;
	private final Predicate<GameState> isGameOver = g -> MAX_ROUND < g.roundCount;
	private final Predicate<GameState> isGameStarted = g -> g.balls.stream().anyMatch((b) -> {return b.vx != 0 || b.vy != 0;});

	public final UnaryOperator<GameState> regularUpdate = g ->
	new GameState(
			g.balls.stream().map(balls.regularUpdate.apply(g)).collect(Collectors.toList()),
			g.blocks.stream().map(blocks.regularUpdate.apply(g))
			.filter(blocks.blockIsLive)
			.collect(Collectors.toList()),

			g.racket.stream().map(racket.regularUpdate.apply(g)).collect(Collectors.toList()),
			g.items.stream().map(items.regularUpdate.apply(g)).collect(Collectors.toList()),
			g.gameSpeed, g.roundCount, g.missed);

	public final UnaryOperator<GameState> leftUpdate = g ->
	new GameState(
			g.balls.stream().map(balls.leftKeyUpdate).collect(Collectors.toList()),
			g.blocks,
			g.racket.stream().map(racket.leftKeyUpdate).collect(Collectors.toList()),
			g.items,
			g.gameSpeed, g.roundCount, g.missed);

	public final UnaryOperator<GameState> rightUpdate = g ->
	new GameState(
			g.balls.stream().map(balls.rightKeyUpdate).collect(Collectors.toList()),
			g.blocks,
			g.racket.stream().map(racket.rightKeyUpdate).collect(Collectors.toList()),
			g.items,
			g.gameSpeed, g.roundCount, g.missed);

	public final UnaryOperator<GameState> spaceUpdate = g ->
	new GameState(
			g.balls.stream().map(balls.spaceKeyUpdate).collect(Collectors.toList()),
			g.blocks,
			g.racket.stream().map(racket.spaceKeyUpdate).collect(Collectors.toList()),
			g.items,
			g.gameSpeed, g.roundCount, g.missed);

	public final UnaryOperator<GameState> enterUpdate = g ->
	new GameState(
			g.balls,
			g.blocks,
			g.racket.stream().map(racket.enterKeyUpdate).collect(Collectors.toList()),
			g.items,
			g.gameSpeed, g.roundCount, g.missed);


	//	public int frameRate(){
	//		return gameSpeed.speed;
	//	}
	//
	//	public void setGameSpeed(GameSpeed gameSpeed){
	//		this.gameSpeed = gameSpeed;
	//	}

	//	@Override
	//	public void update(){
	//		if(missed){
	//			message = "MISS!";
	//			try{ Thread.sleep(800);} catch(Exception e){}
	//			ball.add(new BallLogic(racket.getCenterX()-BallLogic.SIZE/2, racket.getY()-BallLogic.SIZE));
	//			missed = false;
	//			message = "";
	//		}
	//
	//		List<ItemLogic> items = blocks.getItems();
	//
	//		if(key.isState(KeyStates.LEFT)){
	//			racket.moveLeft();
	//		}
	//		else if(key.isState(KeyStates.RIGHT)){
	//			racket.moveRight();
	//		}
	//		racket.update();
	//
	//		//�ܡ���ȯ��or�����ƥ����
	//		if(key.isState(KeyStates.SPACE)){
	//			if(!isGameStarted()){
	//
	//				(ball.get(0)).resetSpeed();
	//			}else{
	//				if(havePowerItem){
	//					for(BallLogic ball : ball){
	//						ball.setPowerMode();
	//					}
	//					havePowerItem = false;
	//				}
	//
	//				if(haveBallItem){
	//					ball.add(new BallLogic(racket.getCenterX()-BallLogic.SIZE/2, racket.getY()-BallLogic.SIZE));
	//					(ball.get(ball.size()-1)).resetSpeed();
	//					haveBallItem = false;
	//				}
	//			}
	//		}
	//
	//		//�����ƥ��ä��Ȥ�
	//		if(ball.size() > 0){
	//			for(ItemLogic item : items){
	//				if(racket.hitCheck(item)){
	//					havePowerItem = false;
	//					haveBallItem = false;
	//
	//					switch(item.getType()){
	//					case POWER:
	//						havePowerItem = true;
	//						break;
	//					case SPEED:
	//						for(BallLogic ball : ball){
	//							ball.setSpeedMode();
	//						}
	//						break;
	//					case BALL:
	//						haveBallItem = true;
	//						break;
	//					case RACKET:
	//						racket.setWideMode();
	//						break;
	//					}
	//
	//					//��ä��Τǥ����ƥ��ä�
	//					items.remove(item);
	//				}
	//			}
	//		}
	//
	//		//�ܡ����update
	//		for(BallLogic ball : ball){
	//			if(ball.isStarted()){
	//				if(ball.isInTheField()){
	//
	//					racket.hitCheck(ball);
	//					blocks.hitCheck(ball);
	//
	//					ball.update();
	//				}
	//				else{
	//					ball.remove(ball);
	//				}
	//			}
	//			else
	//				ball.setLocation(racket.getCenterX()-BallLogic.SIZE/2, racket.getY()-BallLogic.SIZE);
	//		}
	//
	//		blocks.update();
	//
	//		//�ߥ�����update
	//		if(ball.size() < 1){
	//			roundCount++;
	//			racket.endWideMode();
	//			blocks.removeAllItems();
	//			havePowerItem = false;
	//			haveBallItem = false;
	//			missed = true;
	//		}
	//
	//		lastGamesMes = "Balls: "+(MAX_ROUND - roundCount);
	//		existBlocksMes = "Blocks: "+blocks.size();
	//
	//		//�����ཪλ���
	//		if(blocks.size() <= 0){
	//			message = "";
	//			scene = Scene.GAMECLEAR_S;
	//		}
	//
	//		if(!(roundCount < MAX_ROUND)){
	//			message = "";
	//			scene = Scene.GAMEOVER_S;
	//		}
	//	}
	//	@Override
	//	public void draw(Graphics g){
	//		blocks.draw(g);
	//		racket.draw(g);
	//		for(BallLogic ball : ball){
	//			ball.draw(g);
	//		}
	//
	//		g.setColor(Color.black);
	//		g.fillRect(WIDTH-20, HEIGHT-58, 14, 20);
	//
	//		if(havePowerItem){
	//			g.setColor(Color.red);
	//			g.setFont(new Font("Dialog", Font.PLAIN, 20));
	//			g.drawString("P", WIDTH-20, HEIGHT-40);
	//		}
	//
	//		if(haveBallItem){
	//			g.setColor(Color.cyan);
	//			g.setFont(new Font("Dialog", Font.PLAIN, 20));
	//			g.drawString("B", WIDTH-20, HEIGHT-40);
	//		}
	//	}
	//	private boolean isGameStarted(){
	//		for(BallLogic ball : ball){
	//			if(ball.isStarted()) {
	//				return true;
	//			}
	//		}
	//		return false;
	//	}

}