/**
 *
 */
package block.game.manager;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

import block.game.states.Ball;
import block.gui.game.GamePanel;

public final class BallsManager{
	public static final int DEF_VX = 4;
	public static final int DEF_VY = -6;
	public static final int SIZE = 10;

	public BallsManager(){
	}

	public static final Supplier<List<Ball>> newBall = () -> {
		List<Ball> balls = new ArrayList<>();
		balls.add(new Ball(GamePanel.WIDTH/2-SIZE/2, GamePanel.HEIGHT-RacketManager.RACKET_DEF_HEIGHT-SIZE-5,
				0, 0, SIZE, SIZE, true, false, false, 0));
		return balls;
	};

	private final UnaryOperator<Ball> resetSpeed = b ->
		new Ball(b.x, b.y, BallsManager.DEF_VX, BallsManager.DEF_VY, b.width, b.height,
				b.isInTheField, b.isPowerMode, b.isSpeedMode, b.timeCnt);

	private final Predicate<Ball> isStarted = b -> b.vx != 0 || b.vy != 0;

	private final Function<Ball, Predicate<Rectangle>> isXRangeOver = b ->
			r -> !r.intersects(b);

	private final Function<List<Rectangle>, UnaryOperator<Ball>>
		boundX = r -> b -> {
		return new Ball(
				b.x, b.y, -b.vx, b.vy, b.width, b.height,
				b.isInTheField, b.isPowerMode, b.isSpeedMode,
				b.timeCnt);
	};

	private final Predicate<Ball> isYRangeOver = (b) -> {
		return b.y + b.vy <= 0;
	};

	private final Predicate<Ball> isOutOfTheField = (b) -> {
		return b.height <= b.y + b.vy + SIZE;
	};

	private final UnaryOperator<Ball> move = (b) -> {
		return new Ball(
				b.x + b.vx, b.y + b.vy, b.vx, b.vy, b.width, b.height,
				b.isInTheField, b.isPowerMode, b.isSpeedMode,
				b.timeCnt);
	};

	private final UnaryOperator<Ball> takeLeft = b ->
		new Ball(b.x - RacketManager.RACKET_DEF_VX, b.y, b.vx, b.vy, b.width, b.height,
				 b.isInTheField, b.isPowerMode, b.isSpeedMode, b.timeCnt);

	private final UnaryOperator<Ball> takeRight = b ->
		new Ball(b.x + RacketManager.RACKET_DEF_VX, b.y, b.vx, b.vy, b.width, b.height,
				 b.isInTheField, b.isPowerMode, b.isSpeedMode, b.timeCnt);

	private final UnaryOperator<Ball> changeToNormalMode =
			b -> new Ball(b.x, b.y,
						(b.vx > 0) ? DEF_VX : -DEF_VX,
						(b.vy > 0) ? DEF_VY : -DEF_VY,
						b.width, b.height,
						b.isInTheField, false, false, b.timeCnt);

	private final UnaryOperator<Ball> startPowerMode = b ->
			new Ball(b.x, b.y, b.vx * 2, b.vy * 2,
					 b.width, b.height,
					 b.isInTheField, true, false,
					 b.timeCnt, Color.red);

	private final UnaryOperator<Ball> startSpeedMode = b ->
			new Ball(b.x, b.y, b.vx * 3, b.vy * 3,
					b.width, b.height,
					b.isInTheField, false, true,
					b.timeCnt);

	private final UnaryOperator<Ball> timeCountUp = b ->
		 new Ball(b.x, b.y, b.vx, b.vy,
				  b.width, b.height,
				  b.isInTheField, false, false,
				  b.timeCnt+1);

	public final Function<Rectangle, UnaryOperator<Ball>> regularUpdate = r -> b -> {
		Function<Ball, Ball> u = Function.identity();
		final UnaryOperator<Ball> boundX = bb ->
			new Ball(bb.x, bb.y, -bb.vx, bb.vy, bb.width, bb.height,
					bb.isInTheField, bb.isPowerMode, bb.isSpeedMode,
					bb.timeCnt);
		final UnaryOperator<Ball> boundY = bb ->
			new Ball(bb.x, bb.y, bb.vx, -bb.vy, bb.width, bb.height,
					bb.isInTheField, bb.isPowerMode, bb.isSpeedMode,
					bb.timeCnt);

		if(isXRangeOver.apply(b).test(r)){
			u = u.andThen(boundX);
		}
		if(isYRangeOver.test(b)){
			u = u.andThen(boundY);
			if(b.isPowerMode){
				u = u.andThen(changeToNormalMode);
			}
		}
		//		if(h <= y + vy + SIZE)
		//			isInTheField = false;
		//		else
		//			isInTheField =true;

		if(b.isSpeedMode){
			if(b.timeCnt > 50){
				u = u.andThen(changeToNormalMode);
			}
			u = u.andThen(timeCountUp);
		}
		u = u.andThen(move);

		return u.apply(b);
	};

	public final UnaryOperator<Ball> leftKeyUpdate = b -> {
		if(isStarted.negate().test(b)){
			return takeLeft.apply(b);
		}
		return b;
	};

	public final UnaryOperator<Ball> rightKeyUpdate = b -> {
		if(isStarted.negate().test(b)){
			return takeRight.apply(b);
		}
		return b;
	};

	public final UnaryOperator<Ball> spaceKeyUpdate = b -> {
		if(isStarted.negate().test(b)){
			return resetSpeed.apply(b);
		}
		return b;
	};

	//	public Ball(int x, int y){
	//		this.x = x;
	//		this.y = y;
	//		this.w = GamePanel.WIDTH;
	//		this.h = GamePanel.HEIGHT;
	//		isInTheField = true;
	//		isPowerMode = false;
	//		isSpeedMode = false;
	//		vx = 0;
	//		vy = 0;
	//		tcnt = 0;
	//	}

	//	public void resetSpeed(){
	//
	//		vx = DEF_VX;
	//		vy = DEF_VY;
	//	}
	//	public void setPowerMode(){
	//		if(!isPowerMode){
	//			isPowerMode = true;
	//			vx *= 2;
	//			vy *= 2;
	//		}
	//	}
	//
	//	public void setSpeedMode(){
	//		if(!isSpeedMode){
	//			isSpeedMode = true;
	//			vx *= 3;
	//			vy *= 3;
	//			timeCnt = 0;
	//		}
	//	}
	//	@Override
	//	public void update(){
	//		if(x + vx <= 0 || w <= x + vx + SIZE)
	//			boundX();
	//		if(y + vy <= 0){
	//			boundY();
	//			if(isPowerMode)
	//				setNormalMode();
	//		}
	//		if(h <= y + vy + SIZE)
	//			isInTheField = false;
	//		else
	//			isInTheField =true;
	//
	//		if(isSpeedMode){
	//			if(tcnt > 50){
	//				setNormalMode();
	//			}
	//			tcnt++;
	//		}
	//		x += vx;
	//		y += vy;
	//	}
	//	public void boundX(){
	//		vx = -vx;
	//	}
	//
	//	public void boundY(){
	//		vy = -vy;
	//	}
}
