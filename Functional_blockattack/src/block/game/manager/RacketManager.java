/**
 *
 */
package block.game.manager;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;

import block.enums.RacketKeys;
import block.game.states.GameObject;
import block.game.states.Racket;
import block.gui.game.GamePanel;

public class RacketManager{
	public static final int RACKET_DEF_WIDTH  = 80;
	public static final int RACKET_WIDE_WIDTH = (int)(RACKET_DEF_WIDTH * 1.3);
	public static final int RACKET_DEF_HEIGHT = 10;
	public static final int RACKET_WIDE_RANGE = RACKET_WIDE_WIDTH - RACKET_DEF_WIDTH;
	public static final int RACKET_DEF_VX = 6;
	public static final int RACKET_DEF_VY = 0;

	private static final ToIntFunction<Map<String, Object>> getX  = m -> GameObject.getInt.applyAsInt(m, RacketKeys.X);
	private static final ToIntFunction<Map<String, Object>> getY  = m -> GameObject.getInt.applyAsInt(m, RacketKeys.Y);
	private static final ToIntFunction<Map<String, Object>> getVX = m -> GameObject.getInt.applyAsInt(m, RacketKeys.VX);
//	private final ToIntFunction<Map<String, Object>> getVY = m -> GameObject.getInt.applyAsInt(m, RacketKeys.VY);
	private static final ToIntFunction<Map<String, Object>> getWidth = m -> GameObject.getInt.applyAsInt(m, RacketKeys.WIDTH);
	private static final ToIntFunction<Map<String, Object>> getWideRange = m -> GameObject.getInt.applyAsInt(m, RacketKeys.WIDE_RANGE);
	private static final ToIntFunction<Map<String, Object>> getTimeCnt = m -> GameObject.getInt.applyAsInt(m, RacketKeys.TIME_COUNT);
	private static final Predicate<Map<String, Object>> getWideMode = m -> GameObject.getBool.test(m, RacketKeys.WIDE_MODE);

	public static final Function<Map<String, Object>, Racket> newRacket = m ->
		new Racket(getX.applyAsInt(m), getY.applyAsInt(m),
				   getWidth.applyAsInt(m), RACKET_DEF_HEIGHT, getVX.applyAsInt(m),
				   getWideRange.applyAsInt(m), getTimeCnt.applyAsInt(m), getWideMode.test(m));

	public static final Supplier<List<Map<String, Object>>> newDefaultRacket = () -> {
		List<Map<String, Object>> rackets = new ArrayList<>();
		Map<String, Object> res = new HashMap<>();
		res.put(RacketKeys.X.toString(), GamePanel.WIDTH/2-RacketManager.RACKET_DEF_WIDTH/2);
		res.put(RacketKeys.Y.toString(), GamePanel.HEIGHT-RacketManager.RACKET_DEF_HEIGHT-5);
		res.put(RacketKeys.WIDTH.toString(), RacketManager.RACKET_DEF_WIDTH);
		res.put(RacketKeys.Height.toString(), RacketManager.RACKET_DEF_HEIGHT);
		res.put(RacketKeys.VX.toString(), RacketManager.RACKET_DEF_VX);
		res.put(RacketKeys.WIDE_RANGE.toString(), 0);
		res.put(RacketKeys.TIME_COUNT.toString(), 0);
		res.put(RacketKeys.WIDE_MODE.toString(), false);
		rackets.add(res);
		return rackets;
	};

	private final UnaryOperator<Map<String, Object>> moveLeft = h -> {
		h.put(RacketKeys.X.toString(), getX.applyAsInt(h) - getVX.applyAsInt(h));
		return h;
	};

	private final UnaryOperator<Map<String, Object>> moveRight = h -> {
		h.put(RacketKeys.X.toString(), getX.applyAsInt(h) + getVX.applyAsInt(h));
		return h;
	};

	private final UnaryOperator<Map<String, Object>> startWideMode = h -> {
		h.put(RacketKeys.X.toString(), getX.applyAsInt(h) - RACKET_WIDE_RANGE / 2);
		h.put(RacketKeys.WIDTH.toString(), RACKET_WIDE_WIDTH);
		h.put(RacketKeys.WIDE_MODE.toString(), Boolean.TRUE);
		return h;
	};

	public final Function<Rectangle, UnaryOperator<Map<String, Object>>> regularUpdate = rect -> r ->{
		final UnaryOperator<Map<String, Object>> wideModeCountDown = h -> {
			int time = getTimeCnt.applyAsInt(h);
			if(0 < time){
				h.put(RacketKeys.TIME_COUNT.toString(), time - 1);
			}
			return h;
		};
		final Predicate<Map<String, Object>> isWideMode = h -> {
			if(getTimeCnt.applyAsInt(h) <= 0){
				return false;
			}
			return true;
		};
		final UnaryOperator<Map<String, Object>> endWideMode = h -> {
			h.put(RacketKeys.WIDTH.toString(), getX.applyAsInt(h) + RACKET_WIDE_RANGE / 2);
			h.put(RacketKeys.WIDTH.toString(), RACKET_DEF_WIDTH);
			h.put(RacketKeys.WIDE_MODE.toString(), Boolean.FALSE);
			return h;
		};

		wideModeCountDown.apply(r);
		if(isWideMode.negate().test(r)){
			endWideMode.apply(r);
		}
		return r;
	};
	public final UnaryOperator<Map<String, Object>>	rightKeyUpdate = moveRight;
	public final UnaryOperator<Map<String, Object>>	leftKeyUpdate = moveLeft;
	public final UnaryOperator<Map<String, Object>> spaceKeyUpdate = h -> {
		return h;
	};
	public final UnaryOperator<Map<String, Object>>	enterKeyUpdate = h -> {
			return h;
	};


//	public final UnaryOperator<Racket> moveLeft = r ->
//	new Racket(r.x - r.vx, r.y, r.width, r.height,
//		r.vx, r.wideRange, r.timeCnt, r.wideMode);
//
//public final UnaryOperator<Racket> moveRight = r ->
//	new Racket(r.x + r.vx, r.y, r.width, r.height,
//			r.vx, r.wideRange, r.timeCnt, r.wideMode);
//
//final UnaryOperator<Racket> startWideMode = (r) -> {
//	return new Racket(r.x, r.y, r.width, r.height,
//			r.vx, r.wideRange, r.timeCnt, true);
//};
//
//final UnaryOperator<Racket> endWideMode = (r) -> {
//	return new Racket(r.x, r.y, r.width, r.height,
//			r.vx, 0, 0, false);
//};

//	private int x, y, vx, w;
//	private int wideRange, timeCnt;
//	private boolean wideMode;
//	public void moveLeft(){
//	x -= vx;
//}
//	public void moveRight(){
//	x += vx;
//}
//@Override
//public void update(){
//	if(wideMode){
//		wideRange = WIDTH;
//		if(timeCnt > 500){
//			endWideMode();
//		}else{
//			timeCnt++;
//		}
//	}
//
//
//	if(x-(wideRange/2) < 0)
//		x = 0+wideRange/2;
//	if(w < x+WIDTH+wideRange/2)
//		x = w - (WIDTH+(wideRange/2));
//}
//@Override
//public void draw(Graphics g){
//	g.setColor(Color.black);
//	g.fillRect(x+1-(wideRange/2), y+1, WIDTH+wideRange, HEIGHT);
//	g.setColor(Color.red);
//	g.fillRect(x-(wideRange/2), y, WIDTH+wideRange, HEIGHT);
//}
//public void setWideMode(){
//	wideMode = true;
//}
//	public void endWideMode(){
//		wideRange = 0;
//		timeCnt = 0;
//		wideMode = false;
//	}
//	public boolean isHitting(int X, int Y){
//
//		if(x-(wideRange/2) <= X && X <= x+(WIDTH+wideRange/2))
//			if(y <= Y && Y <= y+HEIGHT)
//				return true;
//		return false;
//	}
//
//	public boolean isHittingLeftSide(int X, int Y){
//
//		if(x-(wideRange/2) <= X && X <= x+(WIDTH+wideRange)/5)
//				return true;
//		return false;
//	}
//
//	public boolean isHittingRightSide(int X, int Y){
//
//		if(x+(WIDTH+wideRange)*4/5 <= X && X <= x+(WIDTH+wideRange/2))
//				return true;
//		return false;
//	}
//
//	public void hitCheck(BallLogic ball){
//		if(isHitting(ball.getHCenter(), ball.getBaseLine()+ball.getVy())
//				||isHitting(ball.getHCenter(), ball.getTop()+ball.getVy())){
//			ball.boundY();
//
//			if(isHittingLeftSide(ball.getHCenter(), ball.getBaseLine()+ball.getVy()) && ball.getVx()>0)
//				ball.boundX();
//			else if(isHittingRightSide(ball.getHCenter(), ball.getBaseLine()+ball.getVy())&& ball.getVx()<0)
//				ball.boundX();
//		}
//
//		if(isHitting(ball.getLeft()+ball.getVx(), ball.getVCenter())
//				||isHitting(ball.getRight()+ball.getVx(), ball.getVCenter())){
//			ball.boundX();
//		}
//	}
//
//	public boolean hitCheck(ItemLogic item){
//		return isHitting(item.getX(), item.getY());
//	}
//
//	public boolean hitCheck(int x, int y){
//		return isHitting(x, y);
//	}
}
