/**
 *
 */
package block.game.manager;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

import block.game.states.Item;

public class ItemManager{

	public ItemManager(){
	}

	private final UnaryOperator<Item> move = (i) -> {
		return new Item(i.x + i.vx, i.y + i.vy, i.type);
	};

	public static final Supplier<List<Item>> emptyItems = () -> new ArrayList<>();

	public final Function<Rectangle, UnaryOperator<Item>> regularUpdate = r -> i -> move.apply(i);
}
