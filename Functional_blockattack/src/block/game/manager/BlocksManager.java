/**
 *
 */
package block.game.manager;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;

import block.game.states.Block;

public class BlocksManager{
	public static final int COL = 10;
	public static final int ROW = 9;

	public BlocksManager(){
	}

	public static final Supplier<List<Block>> stage1 = () -> {
		final List<Block> blocks = new ArrayList<>();
		IntStream.range(0, ROW).forEach((x) -> {
			IntStream.range(1, COL).forEach((y) -> {
				blocks.add(new Block(x*Block.WIDTH + 6*(x+1), y*Block.HEIGHT+10*(y+1), y-1));
			});
		});
		return blocks;
	};

	public final Predicate<Block> blockIsLive = b ->  0 <= b.life;
	private final UnaryOperator<Block> attack = b -> new Block(b.x, b.y, b.life - 1);

	public Function<Rectangle, UnaryOperator<Block>> regularUpdate = r -> b -> {
		if(r.intersects(b)){
			attack.apply(b);
		}
		if(blockIsLive.negate().test(b)){
			return null;
		}
		return b ;
	};


//	public BlockLogic(int height){
//		this.height = height;
//		makeStage1();
//		items = new ArrayList<ItemLogic>();
//	}
//	private void makeStage1(){
//		blocks = new ArrayList<Block>();
//		for(int y = 0; y < COL; y++){
//			for(int x = 0; x < ROW; x++){
//				if(y == 0) continue;
//				blocks.add(new Block(x*Block.WIDTH + 6*(x+1), y*Block.HEIGHT+10*(y+1), y-1));
//			}
//		}
//	}
//			public void hitCheck(BallsManager ball){
//			for(Block block : blocks){
//				boolean flg = false;
//				if(block.isHitting(ball.getHCenter(), ball.getTop()+ball.getVy())
//						//�ܡ���ξ����������ä����
//						||block.isHitting(ball.getHCenter(), ball.getBaseLine()+ball.getVy()))
//					//�ܡ���β����������ä����
//				{
//					if(!ball.isPowerMode())
//						ball.boundY();
//					flg = true;
//				}
//				if(block.isHitting(ball.getLeft()+ball.getVx(), ball.getVCenter())
//						//�ܡ���κ�¦�������ä����
//						||block.isHitting(ball.getRight()+ball.getVx(), ball.getVCenter()))
//					//�ܡ���α�¦�������ä����
//				{
//					if(!ball.isPowerMode())
//						ball.boundX();
//					flg = true;
//				}
//				//�����äƤ���ե饰��Ω�äƤ�Τǥ֥�å��õ�
//				if(flg){
//					block.damage++;
//					if(block.damage >= 4){
//						//�����ƥ�����
//						ItemEnum item = ItemEnum.valueOf(new Random().nextInt(15));
//						if(item != null){
//							items.add(new ItemLogic(block.getHCenter(),
//									block.getBaseLine(),
//									item));
//						}
//						blocks.remove(block);
//					}
//				}
//			}
//		}

//		public void update(){
//			for(ItemLogic item : items){
//				item.update();
//				if(item.getY() > HEIGHT) {
//					items.remove(item);
//				}
//			}
//		}
//	public void removeAllItems(){
//		items.clear();
//	}
}
