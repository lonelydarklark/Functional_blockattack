package block.game.states;

public class MessageState{

	public final String centerMessage;
	public final String lastGamesMes;
	public final String existBlocksMes;

	public MessageState(String centerMessage, String lastGamesMes, String existBlocksMes){
		this.centerMessage = centerMessage;
		this.lastGamesMes = lastGamesMes;
		this.existBlocksMes = existBlocksMes;
	}
}
