package block.game.states;

import java.awt.Color;
import java.awt.Graphics;

import block.game.manager.RacketManager;
import block.gui.game.GamePanel;

@SuppressWarnings("serial")
public class Racket extends GameObject{

	public final int wideRange;
	public final int timeCnt;
	public final boolean wideMode;

	public Racket(){
		this(GamePanel.WIDTH/2-RacketManager.RACKET_DEF_WIDTH/2,
				   GamePanel.HEIGHT-RacketManager.RACKET_DEF_HEIGHT-5,
				   RacketManager.RACKET_DEF_WIDTH,
				   RacketManager.RACKET_DEF_HEIGHT,
				   RacketManager.RACKET_DEF_VX,
				   0, 0, false);
	}

	public Racket(int x, int y, int width, int height, int vx, int wideRange, int timeCnt, boolean wideMode) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		this.vx = vx;
		this.vy = 0;
		this.wideRange = wideRange;
		this.timeCnt = timeCnt;
		this.wideMode = wideMode;
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(x + 1, y + 1, width, height);
		g.setColor(Color.red);
		g.fillRect(x, y, width, height);
//		System.out.println(this);
	}

	@Override
	public String toString(){
		return
				"[RACKET: x=" + x + ", y=" + y + ", width=" + width + ", height=" + height
				+ ", vx=" + vx + ", vy=" + vy + ", wideRange=" + wideRange
				+ ", timeCnt=" + timeCnt + ", wideMode=" + wideMode + "]";
	}
}
