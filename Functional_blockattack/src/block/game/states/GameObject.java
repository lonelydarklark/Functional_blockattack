package block.game.states;

import java.awt.Rectangle;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.ToIntBiFunction;

import block.enums.RacketKeys;
import block.interfaces.Drawable;

@SuppressWarnings("serial")
public abstract class GameObject extends Rectangle implements Drawable{

	public int vx;
	public int vy;

	public static final ToIntBiFunction<Map<String, Object>, RacketKeys> getInt = (m, k) -> {
		final Object o = Optional.ofNullable(m.get(k.toString()))
		.orElseThrow(IllegalArgumentException::new);
		return (Integer)o;
	};

	public static final BiPredicate<Map<String, Object>, RacketKeys> getBool = (m, k) -> {
		final Object o = Optional.ofNullable(m.get(k.toString()))
				.orElseThrow(IllegalArgumentException::new);
		return (Boolean)o;
	};

}
