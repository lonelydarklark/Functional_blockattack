package block.game.states;

import java.awt.Color;
import java.awt.Graphics;

@SuppressWarnings("serial")
public final class Ball extends GameObject{
	public final boolean isInTheField;
	public final boolean isPowerMode;
	public final boolean isSpeedMode;
	public final int timeCnt;
	public final Color color;

	public Ball(
			int x,
			int y,
			int vx,
			int vy,
			int width,
			int height,
			boolean isInTheField,
			boolean isPowerMode,
			boolean isSpeedMode,
			int timeCnt){

		this(x, y, vx, vy, width, height, isInTheField, isPowerMode, isSpeedMode, timeCnt, Color.blue);
	}

	public Ball(int x,
			int y,
			int vx,
			int vy,
			int width,
			int height,
			boolean isInTheField,
			boolean isPowerMode,
			boolean isSpeedMode,
			int timeCnt,
			Color color){
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		this.width = width;
		this.height = height;
		this.isInTheField = isInTheField;
		this.isPowerMode = isPowerMode;
		this.isSpeedMode = isSpeedMode;
		this.timeCnt = timeCnt;
		this.color = color;

	}

	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		g.fillArc(x, y, width, height, 0, 360);
//		System.out.println(this);
	}

	@Override
	public String toString(){
		return
				"[BALL: x=" + x + ", y=" + y + ", width=" + width + ", height=" + height
				+ ", vx=" + vx + ", vy=" + vy + "]";
	}
}
