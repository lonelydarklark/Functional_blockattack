package block.game.states;

import java.awt.Graphics;
import java.util.List;
import java.util.Map;

import block.enums.GameSpeed;
import block.game.manager.RacketManager;
import block.gui.game.GamePanel;

@SuppressWarnings("serial")
public class GameState extends AbstractField{

	public final List<Ball> balls;
	public final List<Block> blocks;
	public final List<Map<String, Object>> racket;
	public final List<Item> items;
	public final GameSpeed gameSpeed;
	public final int roundCount;
	public final boolean missed;

	public GameState(
			List<Ball> balls,
			List<Block> blocks,
			List<Map<String, Object>> racket,
			List<Item> items,
			GameSpeed gameSpeed, int roundCount, boolean missed){

		this.balls = balls;
		this.blocks = blocks;
		this.racket = racket;
		this.items = items;
		this.gameSpeed = gameSpeed;
		this.roundCount = roundCount;
		this.missed = missed;

		this.x = 0;
		this.y = 0;
		this.width = GamePanel.WIDTH;
		this.height = GamePanel.HEIGHT;
	}

	@Override
	public String toString(){
		return "balls";
	}

	@Override
	public void draw(Graphics g) {
		blocks.stream().forEach(b -> b.draw(g));
		items.stream().forEach(i -> i.draw(g));
		balls.stream().forEach(b -> b.draw(g));
		racket.stream().map(RacketManager.newRacket).forEach(r -> r.draw(g));
	}
}
