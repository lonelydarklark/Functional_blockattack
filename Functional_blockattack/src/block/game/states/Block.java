/**
 *
 */
package block.game.states;

import java.awt.Color;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class Block extends GameObject{
	public static final int WIDTH = 44;
	public static final int HEIGHT =  10;

	public final int life;

	public Block(int x, int y, int life){
		this.x = x;
		this.y = y;
		this.vx = 0;
		this.vy = 0;
		this.life = life;
	}

	@Override
	public void draw(Graphics g) {
		if(life < 1)
			g.setColor(Color.black);
		else if(life == 1)
			g.setColor(Color.yellow);
		else if(life == 2)
			g.setColor(Color.orange);
		else if(life >= 3)
			g.setColor(Color.red);

		g.fillRect(x, y, WIDTH, HEIGHT);
	}

//	public boolean isHitting(int X, int Y){
//
//		if(x <= X && X <= x+WIDTH)
//			if(y <= Y && Y <= y+HEIGHT){
//				return true;
//			}
//		return false;
//	}
}
