package block.game.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import block.enums.ItemEnum;


@SuppressWarnings("serial")
public class Item extends GameObject {
	public static final int WIDTH = 14;
	public static final int HEIGHT = 20;

	public final ItemEnum type;

	public Item(int x, int y, ItemEnum type){
		this.x = x;
		this.y = y;
		this.width = WIDTH;
		this.height = HEIGHT;
		this.vx = 0;
		this.vy = 4;
		this.type = type;
	}

	@Override
	public void draw(Graphics g) {
		String c;

		g.setColor(Color.black);
		g.fillRect(x, y, width, height);

		switch(type){
		case POWER:
			g.setColor(Color.red);
			c = "P";
			break;
		case SPEED:
			g.setColor(Color.magenta);
			c = "S";
			break;
		case BALL:
			g.setColor(Color.cyan);
			c = "B";
			break;
		case RACKET:
			g.setColor(Color.orange);
			c = "R";
			break;
		default:
			c = "";
			break;
		}

		// 四角の
		g.setFont(new Font("Dialog", Font.PLAIN, 20));
		g.drawString(c, x + 18, y);
	}
}
