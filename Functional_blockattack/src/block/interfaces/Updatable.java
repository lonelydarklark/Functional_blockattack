package block.interfaces;

public interface Updatable {
	public void update();
}
