package block.game.manager;

import block.game.states.Ball;
import junit.framework.TestCase;

public class BallsManagerTest extends TestCase{

	public BallsManagerTest(String test) {
		super(test);
	}

	public void testLeftKeyUpdate(){
		BallsManager bm = new BallsManager();

		Ball expected1 = new Ball(0, 0, 0, 0, 0, 0, false, false, false, 0);
		Ball actual1 = new Ball(-6, 0, 0, 0, 0, 0, false, false, false, 0);
		assertEquals(bm.leftKeyUpdate.apply(expected1), actual1);

		Ball expected2 = new Ball(0, 0, 1, 1, 0, 0, false, false, false, 0);
		Ball actual2 = new Ball(0, 0, 1, 1, 0, 0, false, false, false, 0);
		assertEquals(bm.leftKeyUpdate.apply(expected2), actual2);
}

	public void testRightKeyUpdate(){
		BallsManager bm = new BallsManager();

		Ball expected1 = new Ball(0, 0, 0, 0, 0, 0, false, false, false, 0);
		Ball actual1 = new Ball(6, 0, 0, 0, 0, 0, false, false, false, 0);
		assertEquals(bm.rightKeyUpdate.apply(expected1), actual1);

		Ball expected2 = new Ball(0, 0, 1, 1, 0, 0, false, false, false, 0);
		Ball actual2 = new Ball(0, 0, 1, 1, 0, 0, false, false, false, 0);
		assertEquals(bm.rightKeyUpdate.apply(expected2), actual2);
	}

	public void testSpaceKeyUpdate(){
		BallsManager bm = new BallsManager();

		Ball expected1 = new Ball(0, 0, 0, 0, 0, 0, false, false, false, 0);
		Ball actual1 = new Ball(0, 0, 4, -6, 0, 0, false, false, false, 0);
		assertEquals(bm.spaceKeyUpdate.apply(expected1), actual1);

		Ball expected2 = new Ball(0, 0, 1, 1, 0, 0, false, false, false, 0);
		Ball actual2 = new Ball(0, 0, 1, 1, 0, 0, false, false, false, 0);
		assertEquals(bm.spaceKeyUpdate.apply(expected2), actual2);
	}

}
